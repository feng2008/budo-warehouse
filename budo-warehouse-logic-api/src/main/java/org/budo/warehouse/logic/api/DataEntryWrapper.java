package org.budo.warehouse.logic.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author limingwei
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataEntryWrapper implements DataEntry {
    private DataEntry dataEntry;

    @Override
    public Integer getSourceDataNodeId() {
        return this.getDataEntry().getSourceDataNodeId();
    }

    @Override
    public String getEventType() {
        return this.getDataEntry().getEventType();
    }

    @Override
    public String getSchemaName() {
        return this.getDataEntry().getSchemaName();
    }

    @Override
    public String getTableName() {
        return this.getDataEntry().getTableName();
    }

    @Override
    public Integer getRowCount() {
        return this.getDataEntry().getRowCount();
    }

    @Override
    public Integer getColumnCount(Integer rowIndex) {
        return this.getDataEntry().getColumnCount(rowIndex);
    }

    @Override
    public String getColumnName(Integer rowIndex, Integer columnIndex) {
        return this.getDataEntry().getColumnName(rowIndex, columnIndex);
    }

    @Override
    public Boolean getColumnIsKey(Integer rowIndex, Integer columnIndex) {
        return this.getDataEntry().getColumnIsKey(rowIndex, columnIndex);
    }

    @Override
    public String getColumnValueBefore(Integer rowIndex, Integer columnIndex) {
        return this.getDataEntry().getColumnValueBefore(rowIndex, columnIndex);
    }

    @Override
    public String getColumnValueAfter(Integer rowIndex, Integer columnIndex) {
        return this.getDataEntry().getColumnValueAfter(rowIndex, columnIndex);
    }
}