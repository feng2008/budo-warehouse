package org.budo.warehouse.logic.util;

import org.budo.warehouse.logic.api.DataEntry;

/**
 * @author limingwei
 */
public class DataEntryUtil {
    public static boolean hasIsKeyColumn(DataEntry dataEntry, Integer rowIndex) {
        Integer columnCount = dataEntry.getColumnCount(rowIndex);

        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            Boolean columnIsKey = dataEntry.getColumnIsKey(rowIndex, columnIndex);
            if (columnIsKey) {
                return true;
            }
        }

        return false;
    }
}